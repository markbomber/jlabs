using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace CompanyWebPage.Tests
{
    [TestClass]
    public class ErrorPageTest : BaseTest
    {
        private string ErrorPageSomeContext = "<h1>There is an error on the page or a page not exists!</h1>";

        [TestMethod]
        public async Task CheckErrorUrlTest()
        {
            using (var factory = new CustomWebApplicationFactory<CompanyWebPage.Web.Startup>())
            using (var client = factory.CreateClient())
            {
                var result = await client.SendAsync(PrepareGetRequest("/error", CultureEnglish));
                result.EnsureSuccessStatusCode();
                var content = await result.Content.ReadAsStringAsync();
                Assert.IsTrue(content.Contains(ErrorPageSomeContext));             
            }
        }

        [TestMethod]
        public async Task CheckErrorWhenNotExistingAbout123UrlTest()
        {
            using (var factory = new CustomWebApplicationFactory<CompanyWebPage.Web.Startup>())
            using (var client = factory.CreateClient())
            {
                var result = await client.SendAsync(PrepareGetRequest("/about123", CultureEnglish));
                Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode);
                var content = await result.Content.ReadAsStringAsync();
                Assert.IsTrue(content.Contains(ErrorPageSomeContext));
            }
        }
    }
}
