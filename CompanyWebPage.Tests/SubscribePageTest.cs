using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace CompanyWebPage.Tests
{
    [TestClass]
    public class SubscribePageTest : BaseTest
    {
        private string SubscribeCharacteristicEnglishContent = "<h1>Subscribe to newsletter</h1>";
        private string SubscribeCharacteristicSpanishContent = "<h1>Suscríbase al boletín";

        [TestMethod]
        public async Task CheckSubscribeUrlEnglishCultureInHeaderTest()
        {
            using (var factory = new CustomWebApplicationFactory<CompanyWebPage.Web.Startup>())
            using (var client = factory.CreateClient())
            {
                var result = await client.SendAsync(PrepareGetRequest("/newsletter/subscribe", CultureEnglish));
                result.EnsureSuccessStatusCode();
                var content = await result.Content.ReadAsStringAsync();
                Assert.IsTrue(content.Contains(SubscribeCharacteristicEnglishContent));
            }
        }

        [TestMethod]
        public async Task CheckSubscribeUrlGermanCultureInHeaderTest()
        {
            using (var factory = new CustomWebApplicationFactory<CompanyWebPage.Web.Startup>())
            using (var client = factory.CreateClient())
            {
                var result = await client.SendAsync(PrepareGetRequest("/newsletter/subscribe", CultureGerman));
                result.EnsureSuccessStatusCode();
                var content = await result.Content.ReadAsStringAsync();
                Assert.IsTrue(content.Contains(SubscribeCharacteristicEnglishContent)); // for german culture should return english page
            }
        }

        [TestMethod]
        public async Task CheckSubscribeUrlEnglishCultureInUrlTest()
        {
            using (var factory = new CustomWebApplicationFactory<CompanyWebPage.Web.Startup>())
            using (var client = factory.CreateClient())
            {
                var result = await client.SendAsync(PrepareGetRequest($"/newsletter/subscribe?culture={CultureEnglish}", ""));
                result.EnsureSuccessStatusCode();
                var content = await result.Content.ReadAsStringAsync();
                Assert.IsTrue(content.Contains(SubscribeCharacteristicEnglishContent));
            }
        }

        [TestMethod]
        public async Task CheckSubscribeUrlSpanishCultureInHeaderTest()
        {
            using (var factory = new CustomWebApplicationFactory<CompanyWebPage.Web.Startup>())
            using (var client = factory.CreateClient())
            {
                var result = await client.SendAsync(PrepareGetRequest("/newsletter/subscribe", CultureSpanish));
                result.EnsureSuccessStatusCode();
                var content = await result.Content.ReadAsStringAsync();
                Assert.IsTrue(content.Contains(SubscribeCharacteristicSpanishContent));
            }
        }

        [TestMethod]
        public async Task CheckSubscribeUrlSpanishCultureInUrlTest()
        {
            using (var factory = new CustomWebApplicationFactory<CompanyWebPage.Web.Startup>())
            using (var client = factory.CreateClient())
            {
                var result = await client.SendAsync(PrepareGetRequest($"/newsletter/subscribe?culture={CultureSpanish}", ""));
                result.EnsureSuccessStatusCode();
                var content = await result.Content.ReadAsStringAsync();
                Assert.IsTrue(content.Contains(SubscribeCharacteristicSpanishContent));
            }
        }
    }
}
