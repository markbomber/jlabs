using CompanyWebPage.Resources;
using System.ComponentModel.DataAnnotations;

namespace CompanyWebPage.ViewModel
{
    public class NewsletterSubscribeViewModel
    {
        [Display(Name = "YourAge", ResourceType = typeof(SharedResources))]
        [Range(1, 99, ErrorMessageResourceName = "AgeIsIncorrect", ErrorMessageResourceType = typeof(SharedResources))]
        public int? Age { get; set; }

        [Display(Name = "YourEmailAddress", ResourceType = typeof(SharedResources))]
        [Required(ErrorMessageResourceName = "EmailCanNotBeEmpty", ErrorMessageResourceType = typeof(SharedResources))]
        public string EmailAddress { get; set; }

        [Display(Name = "YourFirstName", ResourceType = typeof(SharedResources))]
        [Required(ErrorMessageResourceName = "FirstNameCanNotBeEmpty", ErrorMessageResourceType = typeof(SharedResources))]
        public string FirstName { get; set; }
    }
}
