﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using CompanyWebPage.Log;

namespace CompanyWebPage.Web.Filters
{
    public class RequestLoggingFilter : IActionFilter
    {
        private readonly IActionLogger _actionLogger;

        public RequestLoggingFilter(IActionLogger actionLogger)
        {
            _actionLogger = actionLogger;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var controller = GetRouteValue(context, "controller");
            var action = GetRouteValue(context, "action");
            var actionParameters = context.ActionArguments
                .Select(x => new ActionParameter
                {
                    ParameterName = x.Key,
                    ParameterType = x.GetType(),
                    ParameterValue = x.Value,
                })
                .ToList();

            _actionLogger.Log(controller, action, actionParameters);
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        private static string GetRouteValue(ActionExecutingContext context, string name) =>
            context.RouteData.Values.TryGetValue(name, out var routeValue) ? routeValue?.ToString() : null;
    }
}
