using Microsoft.AspNetCore.Mvc;

namespace CompanyWebPage.Web.Controllers
{
    public class AboutController : Controller
    {
        public IActionResult About()
        {
            return View();
        }
    }
}
