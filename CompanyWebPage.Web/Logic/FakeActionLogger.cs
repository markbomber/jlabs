using CompanyWebPage.Log;
using System.Collections.Generic;

namespace CompanyWebPage.Web.Logic
{
    public class FakeActionLogger : IActionLogger
    {
        public void Log(string controllerName, string actionName, IList<ActionParameter> actionParameters)
        {
            // do nothing
        }
    }
}
